<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

// Implement JWTSubject interface
class User extends Authenticatable implements JWTSubject
{
  // BODY OF THIS CLASS
  public function getJWTIdentifier()
  {
    return $this->getKey();
  }
  
  public function getJWTCustomClaims()
  {
    return [];
  }
}